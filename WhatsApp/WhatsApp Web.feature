#language: pt
Funcionalidade: WhatsApp Web
Como usuário
Eu quero utilizar a funcionalidade WhatsApp Web
Para que eu receba as mensagens via browser web

Cenário: Configurar o WhatsApp Web com o device offline.
Dado que eu esteja com o device sem conexão à internet
E que eu esteja na aba "Conversas"
E abra o menu de opções
E escolha a opção WhatsApp Web
Quando clico no botão "+"
Então deve exibir uma mensagem de erro "Você não possui acesso à rede no momento."

Cenário: Configurar o WhatsApp Web com o device online
Dado que eu esteja com o device com conexão ativa de internet
E que eu esteja na aba "Conversas"
E aba o menu de opções
E escolha a opção WhatsApp Web
Quando clico no botão "+"
Então deve exibir a tela de escanear código
