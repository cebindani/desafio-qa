#language: pt
Funcionalidade: Alterar Status
Eu quero alterar minha mensagem de Status
Para indicar aos meus contatos o que estou fazendo

Cenário: Status vazio
Dado que eu esteja na tela principal
E e abra o menu de opções
E escolha a opção Status
Quando toco no ícone de lápis
  E deixo o campo de texto vazio
  E confirmo no botão "Ok"
Então deve exibir mensagem de erro "Status não pode estar vazio".

Cenário: Escolher um status padrão.
Dado que eu esteja na tela principal
E e abra o menu de opções
E escolha a opção Status
E possua uma listagem de status
Quando escolho um status da listagem de status.
Então o status deve ser alterado.

Cenário: Criar um status personalizado.
Dado que eu esteja na tela principal
E e abra o menu de opções
E escolha a opção Status
Quando toco no ícone de lápis
  E escrevo o novo status com até 139 caracteres
  E confirmo no botão "Ok"
Então o status deve ser alterado.