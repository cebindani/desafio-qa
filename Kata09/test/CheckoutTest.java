import com.cebin.Checkout;
import com.cebin.PricingRules;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by DanieleM on 30/06/2016.
 */
public class CheckoutTest {


    List<PricingRules> rules = new ArrayList<>();

    @Before
    public void setUp() {
        rules.clear();

        rules.add(new PricingRules("A", 50, 3, 130));
        rules.add(new PricingRules("B", 30, 2, 45));
        rules.add(new PricingRules("C", 20));
        rules.add(new PricingRules("D", 15));
    }

    @Test
    public void testTotal() throws Exception {

        Checkout checkout = new Checkout(rules);
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("A");

        assertEquals(180, checkout.total());

    }

    @Test(expected = Exception.class)
    public void testTotalProductWithoutPrice() throws Exception {

        Checkout checkout = new Checkout(rules);
        checkout.scan("A");
        checkout.scan("B");
        checkout.scan("C");
        checkout.scan("D");
        checkout.scan("E");

        assertEquals(115, checkout.total());

    }

}