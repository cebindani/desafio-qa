import com.cebin.Checkout;
import com.cebin.PricingRules;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by DanieleM on 30/06/2016.
 */
public class TestPrice {


    List<PricingRules> rules = new ArrayList<>();


    @Before
    public void cleanRules() {
        rules.clear();
        rules.add(new PricingRules("A", 50, 3, 130));
        rules.add(new PricingRules("B", 30, 2, 45));
        rules.add(new PricingRules("C", 20));
        rules.add(new PricingRules("D", 15));
    }


    private int price(String products) throws Exception {


        Checkout checkout = new Checkout(rules);

        for (int i = 0; i < products.length(); i++) {
            checkout.scan(Character.toString(products.charAt(i)));
        }

        return checkout.total();

    }


    @Test
    public void testTotals() throws Exception {

        assertEquals(0, price(""));
        assertEquals(50, price("A"));
        assertEquals(80, price("AB"));
        assertEquals(115, price("CDBA"));

        assertEquals(100, price("AA"));
        assertEquals(130, price("AAA"));
        assertEquals(180, price("AAAA"));
        assertEquals(230, price("AAAAA"));
        assertEquals(260, price("AAAAAA"));

        assertEquals(160, price("AAAB"));
        assertEquals(175, price("AAABB"));
        assertEquals(190, price("AAABBD"));
        assertEquals(190, price("DABABA"));
    }

    @Test
    public void testIncremental() throws Exception {


        Checkout checkout = new Checkout(rules);

        assertEquals(0, checkout.total());
        checkout.scan("A");
        assertEquals(50, checkout.total());
        checkout.scan("B");
        assertEquals(80, checkout.total());
        checkout.scan("A");
        assertEquals(130, checkout.total());
        checkout.scan("A");
        assertEquals(160, checkout.total());
        checkout.scan("B");
        assertEquals(175, checkout.total());
    }

}