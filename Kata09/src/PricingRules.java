package com.cebin;

/**
 * Created by DanieleM on 30/06/2016.
 */
public class PricingRules {

    private String product;
    private int unitPrice;
    private int quantityForSpecialPrice = 0;
    private int specialPrice = 0;
    private boolean hasSpecialPrice;


    public PricingRules(String product, int unitPrice, int quantityForSpecialPrice, int specialPrice) {

        this.product = product;
        this.unitPrice = unitPrice;
        this.quantityForSpecialPrice = quantityForSpecialPrice;
        this.specialPrice = specialPrice;
        this.hasSpecialPrice = true;
    }



    public PricingRules(String product, int unitPrice) {

        this.product = product;
        this.unitPrice = unitPrice;
        this.hasSpecialPrice = false;
    }

    public boolean isSpecialPrice() {
        return hasSpecialPrice;
    }

    public String getProduct() {
        return product;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public int getQuantityForSpecialPrice() {
        return quantityForSpecialPrice;
    }

    public int getSpecialPrice() {
        return specialPrice;
    }
}
