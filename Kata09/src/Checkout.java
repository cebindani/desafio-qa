package com.cebin;

import java.util.HashMap;
import java.util.List;


public class Checkout {


    private HashMap<String, PricingRules> rules = new HashMap<>();
    private HashMap<String, Integer> shoppingCart = new HashMap<>();


    public Checkout(List<PricingRules> checkoutRules) {

        for (PricingRules productRule : checkoutRules) {
            rules.put(productRule.getProduct(), productRule);
        }
    }

    public int total() throws Exception {

        int total = 0;

        for (String item :
                shoppingCart.keySet()) {

            int subtotal = 0;
            if (!rules.containsKey(item)) {
                shoppingCart.remove(item);
                throw new Exception("Product not found in pricing rules. Removing from shopping cart");
            } else {
                int units = shoppingCart.get(item);
                PricingRules pricingRuleForItem = rules.get(item);


                final int unitPrice = pricingRuleForItem.getUnitPrice();
                if (!pricingRuleForItem.isSpecialPrice()) {
                    subtotal = units * unitPrice;
                } else {
                    final int quantityForSpecialPrice = pricingRuleForItem.getQuantityForSpecialPrice();
                    if (units < quantityForSpecialPrice) {
                        subtotal = units * unitPrice;
                    } else if (units >= quantityForSpecialPrice) {
                        int normalPriceItens = units % quantityForSpecialPrice;
                        int specPriceItens = (units - normalPriceItens) / quantityForSpecialPrice;
                        final int specialPrice = pricingRuleForItem.getSpecialPrice();

                        subtotal = normalPriceItens * unitPrice + specPriceItens * specialPrice;
                    }
                }
            }

            total += subtotal;
        }
        return total;
    }

    public void scan(String product) {

        if (shoppingCart.containsKey(product)) {
            int value = shoppingCart.get(product);
            shoppingCart.put(product, value + 1);
        } else
            shoppingCart.put(product, 1);

    }
}